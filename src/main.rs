use dotenv::dotenv;
use std::env;
use std::fs::File;
use std::fs;
use std::io::Read;
use std::io::prelude::*;
use std::path::Path;

struct Emote {
    code: String,
    id: String,
    user: String,
}

fn main() {
    dotenv().ok();

    let argv: Vec<String> = env::args().collect();
    let user = &argv[1];
    let download_root = &argv[2];

    let client_id = "TWITCH_CLIENT";
    let client_token = "TWITCH_TOKEN";

    let twitch_client_id = match env::var(client_id) {
        Ok(val) => val,
        Err(_) => panic!("Missing {} environment variable", client_id),
    };

    let twitch_client_token = match env::var(client_token) {
        Ok(val) => val,
        Err(_) => panic!("Missing {} environment variable", client_token),
    };

    let id = get_id(&user, &twitch_client_id, &twitch_client_token);
    let emotes = get_emotes(&id);

    download_emotes(&emotes, download_root);
}

fn get_id<'a>(user: &str, key: &str, sec: &str) -> String {
    let url_string = format!("https://api.twitch.tv/helix/users?login={}", &user);
    let auth_header = format!("Bearer {}", &sec);

    let blob = match ureq::get(&url_string)
        .set("Authorization", &auth_header)
        .set("Client-ID", &key)
        .call()
        .into_json() {
            Ok(blob) => blob,
            Err(err) => panic!("Failed to fetch ID for {}: {:?}",
                               &user,
                               err),
        };

    return String::from(blob["data"][0]["id"].as_str().unwrap())
}

fn get_emotes(id: &str) -> Vec<Emote> {
    let url_string = format!("https://api.twitchemotes.com/api/v4/channels/{}", &id);

    let blob = match ureq::get(&url_string)
        .call()
        .into_json() {
            Ok(blob) => blob,
            Err(err) => panic!(
                "Failed to fetch emote data for {}: {:?}",
                &id,
                err
            ),
        };

    let user = blob["channel_name"].as_str().unwrap();

    // println!("blob is {:?}", &blob);
    return blob["emotes"]
        .as_array()
        .unwrap()
        .iter()
        .map(|v|
             Emote {
                 code: String::from(v["code"].as_str().unwrap()),
                 // is a number in the JSON
                 id: v["id"].to_string(),
                 user: String::from(user),
             }
        ).collect()
}

fn download_emotes(emotes: &Vec<Emote>, download_root: &str) -> () {
    for emote in emotes {
        let dir = Path::new(download_root).join(&emote.user);
        fs::create_dir_all(&dir).expect("scooby voice");

        let emote_path = dir.join(format!("{}.jpg", &emote.code));

        println!("Writing {}", emote_path.display());

        let url_string = format!("https://static-cdn.jtvnw.net/emoticons/v1/{}/3.0", emote.id);
        let mut reader = ureq::get(&url_string).call().into_reader();
        let mut bytes = Vec::new();
        reader.read_to_end(&mut bytes).expect("wiggle");

        let mut file = match File::create(&emote_path) {
            Err(why) => panic!("couldn't create {}: {}", emote_path.display(), why),
            Ok(file) => file,
        };
        file.write_all(&bytes).expect("bonk");
    }
}
